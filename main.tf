resource "google_compute_disk" "default" {
  name  = var.name_disk
  type  = var.type
  zone  = var.zone
  image = var.image
  size = var.size
  labels = {
    environment = "dev"
  }
  physical_block_size_bytes = 4096
}

resource "google_compute_attached_disk" "default1" {
  disk     = google_compute_disk.default.id
  instance = google_compute_instance.vm.id
}

resource "google_compute_instance" "vm" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.image
      size = 10
    }
  }

  network_interface {
    network = "default"
  }

  lifecycle {
    ignore_changes = [attached_disk]
  }
}