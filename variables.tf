variable "project" {
  type = string
}
variable "region" {
  type = string
}
variable "name_disk" {}
variable "type" {}
variable "zone" {}
variable "image" {}
variable "size" {}
variable "name" {}
variable "machine_type" {}